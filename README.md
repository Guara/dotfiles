# Dot Files
Usage
-----

Pull the repository and [use chezmoi.io](https://chezmoi.io).

```sh
chezmoi init git@gitlab.com:Guara/dotfiles.git
chezmoi apply
```

To pull and apply latest update
```
chezmoi update
```

Included config
-----
-  [fish](https://fishshell.com)
-  [nushell](https://www.nushell.sh/)
-  [Oh My Fish](https://github.com/oh-my-fish/oh-my-fish)
-  [swaywm](https://swaywm.org/)
-  [wofi](https://hg.sr.ht/~scoopta/wofi)
-  [alacritty](https://github.com/alacritty/alacritty/)
-  [i3status-rust](https://github.com/greshake/i3status-rust/)
-  [Polybar](https://github.com/polybar/polybar) (incomplete)
-  [Waybar](https://github.com/Alexays/Waybar) (incomplete)
-  [logiops](https://github.com/PixlOne/logiops)
-  [cargo](https://github.com/rust-lang/cargo/)
-  [pipewire](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/home)
-  [pipewire-pulse](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Config-PulseAudio)
