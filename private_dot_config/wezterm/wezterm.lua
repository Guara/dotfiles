local wezterm = require 'wezterm';
return {
	window_decorations = "RESIZE",
	hide_tab_bar_if_only_one_tab = true,
		window_frame = {
    inactive_titlebar_bg = '#365e67',
    active_titlebar_bg = '#273c41',
    inactive_titlebar_fg = '#000000',
    active_titlebar_fg = '#ffffff',
    inactive_titlebar_border_bottom = '#273c41',
    active_titlebar_border_bottom = '#273c41',
    button_fg = '#000000',
    button_bg = '#273c41',
    button_hover_fg = '#ffffff',
    button_hover_bg = '#273c41',
  },
	font = wezterm.font("FiraCode Nerd Font"),
	font_size = 14,
	color_scheme = 'Solarized Dark Higher Contrast',
	window_background_opacity = .85,
	enable_scroll_bar = false,
	enable_wayland = true,
	front_end = "WebGpu",	
}
