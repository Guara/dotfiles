use std/util "path add"
#
# Config
# 
$env.config.show_banner = false
$env.config.completions.algorithm = "fuzzy"  # prefix or fuzzy
$env.config.use_kitty_protocol = true
$env.config.buffer_editor = "hx"
#
# Env
# 
$env.GOPATH = ($env.HOME | path join "workspace")
$env.GOBIN = ($env.HOME | path join "workspace/bin")
$env.PROMPT_COMMAND = "starship prompt"
$env.EDITOR = 'hx'
$env.CARGO_HOME = ($env.HOME | path join ".cargo")
# $env.EGL_PLATFORM = 'wayland'
# $env.MOZ_ENABLE_WAYLAND = 1
# $env.VDPAU_DRIVER = 'radeonsi'
# $env.LIBVA_DRIVER_NAME = 'radeonsi' 
$env.GIT_WORKSPACE = '~/dev'
source ~/.config/nushell/custom-env.nu 
#
# Path
# 
path add '/usr/local/bin'
path add '/usr/local/sbin'
path add '/usr/bin'
path add '/usr/sbin'
path add $env.GOBIN
path add ($env.HOME | path join ".local/bin")
path add ($env.HOME | path join "bin")
path add ($env.HOME | path join "go/bin")
path add ($env.HOME | path join ".krew/bin")
path add ($env.CARGO_HOME | path join "bin")

$env.LS_COLORS = (vivid generate molokai)
#
# Starship
# 
mkdir ($nu.data-dir | path join "vendor/autoload")
starship init nu | save -f ($nu.data-dir | path join "vendor/autoload/starship.nu")

# Misc
source /home/dani/.config/broot/launcher/nushell/br
